/** @format */

import React from 'react';

import Layout from '../components/layout';
import SEO from '../components/seo';

const About = ({ location }) => (
	<Layout location={location}>
		<SEO title="About Us" />
		<h1>About Us</h1>
		<p>
			Health goth retro man braid, occupy lomo raclette keffiyeh copper mug 90's pug
			mumblecore. Irony next level tacos disrupt slow-carb freegan, intelligentsia tofu ugh.
			Irony ennui flexitarian brunch coloring book listicle scenester yr iPhone vape
			meditation. Blue bottle umami single-origin coffee, microdosing gochujang whatever
			mixtape subway tile. PBR&B hoodie viral, chicharrones hammock keffiyeh sustainable chia
			lyft. Irony cold-pressed tacos chicharrones, iceland umami gochujang bicycle rights.
		</p>

		<p>
			Gluten-free sartorial bespoke vice edison bulb. Biodiesel bicycle rights crucifix
			vexillologist. Cliche banh mi four dollar toast master cleanse, kombucha narwhal squid
			gluten-free drinking vinegar tacos poutine pitchfork YOLO 90's meditation. Listicle
			messenger bag aesthetic blog fam kitsch tilde keffiyeh polaroid mumblecore cronut.
			Coloring book authentic tacos kombucha before they sold out fixie. Prism synth
			cold-pressed shabby chic ennui PBR&B viral taxidermy shoreditch portland migas craft
			beer fingerstache.
		</p>

		<p>
			Organic before they sold out authentic hoodie fanny pack synth. Paleo listicle activated
			charcoal marfa kogi pop-up everyday carry. Readymade yr art party schlitz. Vegan
			leggings venmo copper mug cornhole deep v listicle, cronut kogi helvetica. YOLO
			distillery bitters actually ennui crucifix. Cloud bread woke live-edge master cleanse.
		</p>
	</Layout>
);

export default About;
